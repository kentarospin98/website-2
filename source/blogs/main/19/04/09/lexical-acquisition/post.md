# Lexical Acquisition Through Category Matching: 12-Month-Old Infants Associate Words to Visual Categories

## PSY 101 Journal Report

*Lexical Acquisition* is the process in which children learn new words, and associate then with new meanings. 
Children often have a conception understanding of an object, and can recognise the object, before learning the word for it.
They create categories in their mind for the objects they've seen, called *preverbal categories*.
These preverbal categories could be used as by the infants to learn new words by associating the categories with lexical labels through a process called *Category Matching*. [@cat]
@lex tried to answer the question if infants utilize Category Matching in order to learn words through three experiments.

### Experiment 1

The first experiment tried to answer if infants ability to recognise an object by its name improved when the infants learnt the name through category matching.
They divided the infants in two categories.
The infants were shown 16 pictures of two objects (8 for each type) they were unfamiliar with.
This was the familiarization phase.
In this case, they were Staplers and Coffee makers.
The difference between the two groups was that the in infants in experimental group where shown the pictures in blocks.
That is, first all the pictures of one category were displayed, followed by all the pictures from the second category.
This was called the *blocked-category group*.
The infants in the second second group was shown the pictures such that the pictures where randomised, with no more than two pictures of a category being displayed in a row.
This was called the *interleaved-category group*.

Then the infants where shown two more pictures from each category, with a recording of a woman saying the name of the object, along with some carrier phrases.
Finally, pictures of both the categories were shown at the same time, with a recording of a woman asking the infant to look at a object.
The researchers then used eye tracking equipment to measure how if the infant looked at the target object.

What the researchers found was that the infants in the children in the blocked-category group spent more time looking at the target area, after naming, than infants in the interleaved-category group.
This suggested that the preverbal categories were utilized by the infants during the naming process.

### Experiment 2

In order to settle that the infants depended on preverbal categories in order to generalize words to similar objects, a second experiment was conducted.
The second experiment was similar to the first one, except this time, the infants weren't shown any pictures prior to the naming phase.
The researchers also decided on administering two types of tests.
The first one was the same as experiment 1.
In the second test, instead of using random pictures of objects from each category, the same pictures that were used in the naming phase were used again.

The researchers found that the infants did not spend more time looking at the target postnaming.
This suggested that the category knowledge was a prerequisite for lexical acquisition.

### Experiment 3

The researchers were considering the possibility that during the first experiment, the infants in the interleaved-category group were creating a preverbal super-category with both kinds of objects in their mind, and had made to learn two different words for the same super-category.
And that the reason the infants weren't able to learn the words for those categories was due to a phenomenon called *mutual exclusivity*, where infants show resistance in learning two words for a single object, as demonstrated by @mut.

In the third experiment, the infants were once again divided into the two block-category and interleaved-category groups.
This time, though, a third type of object, unfamiliar to the infant was introduced: garlic press.
This was the *novel category*.
There were two phases in this experiment.
The first phase was the familiarization phase, which was the same as in experiment 1.
The second phase was the test phase.
In this phase, two images were shown on the screen: one new image of either a stapler of coffee press, and a second picture of a garlic press.
The researchers expected the infants to spend more time looking at the novel category, due to a phenomenon known as novelty preference. [@fer]

### Conclusion

The researchers provided evidence supporting their hypothesis that infants learn new words through the utilization of Category Matching.
They also provided evidence that suggested that infants have a difficult in learning words without Category Matching, though they did admit that there might be other processes that infants might use for Lexical Acquisition.
Of course, more research is needed.
The sample size of this study was quite small.
The infants were all from monolingual homes.
It would be interesting to see how exposure to multiple languages in infancy would affect learning.

::: {#refs}

# References

:::
