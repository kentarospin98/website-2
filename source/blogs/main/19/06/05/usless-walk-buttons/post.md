# The useless walk buttons.

## Buckle up, it's rant time.

It didn't take too long after moving to America for me to realize that the "Push Button To Cross" buttons on intersections don't actually work.
This might not be the case in every city, but at least in Tempe (and Phoenix, Mesa, Gilbert, Chandler, etc.) these buttons are pretty much non-functional.
You can press these buttons all you want. 
The most you'll get from them is a cold and heartless "Wait."

A quick search on the *Interwebs* led me to a nice article written by @bos that explains why these buttons exist and function the way they do. 
Here's the TL;DR version:

Turns out these buttons did work back when there weren't all that many cars on the road.
Pressing the button would either instantly turn the lights red, or would signal the intersection and make the overall wait time lower.
But as the volume of traffic and pedestrians in cities increased, it didn't make sense for pedestrians to have the ability to be able to control the flow of traffic.
Cities started disabling the buttons, and most of these ended up nonfunctional.
Most intersections now just assume there's always a pedestrian waiting at the intersection to cross.

The reason cities don't remove these buttons is because it would cost the city too much.
Furthermore, a lot of buttons also act at aural and haptic indicators for the disabled.

This doesn't change the fact that these buttons are still extremely annoying.
While pressing the button doesn't make the intersection open up for you, on some intersections, such as on Southern Ave./Dobson Rd. in Mesa, *not* pressing the button means the walk sign doesn't light up.
And there's no way to tell unless you've been at that intersection before and had that happen to you, so you might as well press every button, even when you know it's usually pointless.

Besides, we could just have the audio indicators on intersections without the buttons.
Having the buttons there is just pointless, and creates more problems than it solves.

::: {#refs}

## References

:::

### Copyright &copy; 2019, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)

