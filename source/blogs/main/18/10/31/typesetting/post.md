# Typesetting

## Adapted from my Mastodon account

So, when Luke Smith introduced Groff, I felt he was going too far, and it was too clunky and complicated. 
But as I'm reading more about it, I'm getting more and more interested. 
I can see this being super useful combined with a continuous serial printer. 
I'm gonna try it out for some real typesetting work and see how it works for me. Bringing out latex for simple documents is overkill. I'm not sure how it will compete with markdown.

I need to write more to practice my English. Hated writing in school (because anarchy :thinkergunsunglasses: )
I use markdown pretty much everywhere. But I can imagine Groff being useful to generate documents with scripts, quickly pipe stdin to create documents, stuff like that. 
I still really like markdown, simply because it's readable in its raw form. 
Groff produces really nice console ASCII / ANSI output. That's why it would pair perfectly with a serial dot matrix

I've tried out the eqn preprocessor and damn is it good. 
Groff/troff is perfect for situations where latex would be too complicated and markdown would be too limiting. 
Also there's a guide written by Brian Kernighan!

ODF is a document format. Groff/Troff/Latex are typesetting tools. Markdown/AsciiDoc are markup languages. 
They all have different uses.
I like ODF when compared to DOC but i wouldn't use it for typesetting or taking notes. It would be a pain. I cant even put it under source control. 
What i do often is write in markdown/latex (Depending on what i'm doing) and then convert to ODF, PDF and HTML.

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)

