# Free Software

## And why you should pay for it.

There's a huge misunderstanding in the Free, Libre, and Open Source community (and in the general public) about what "Free Software" means. I am hoping by the end of the blog post I can clarify at least some of the confusion.

### Software Licensing

When a developer decides to publish a piece of software, that they own the copyright to, they have to decide how they want to license it. What license they decide to use will determine the freedoms, rights, and limitations they have decided to impose on the users of that program.
Licensing software is important. It lets the users know what their rights are. If a copyright holder publishes code without licensing it, anyone who has access to that code must ask for permission before using that code. This includes running it, distributing it, or using that code in their own projects.
There are 4 main types of software licenses:

- Proprietary
- Non-Free Open Source
- Copy-Left Open Source
- Permissive Open Source

### Proprietary Software

Proprietary Software is software that does not allow the user access to the source code.[^sourcecode] This means the program is often compiled into binaries (or executables), or obfuscated.[^obfus] Proprietary software licenses also often restrict the user from distributing the software, even if the piece of software is free of charge. User do not have the freedom to modify the software, audit the software and they cannot sell the software. Hence the user cannot trust the software as not being malicious. Microsoft Windows, Apple Mac OS, and Apple iOS are all proprietary software.

[^sourcecode]: The Linux Information Project [Source Code Definition](http://www.linfo.org/source_code.html).

	Source code (also referred to as source or code) is the version of software as it is originally written (i.e., typed into a computer) by a human in plain text (i.e., human readable alphanumeric characters).

[^obfus]: Techopedia [Obfuscation](https://www.techopedia.com/definition/16375/obfuscation)

	Obfuscation is a programming technique in which code is intentionally obscured to prevent reverse engineering and deliver unclear code to anyone other than the programmer. Obfuscation is also applied to programs to ensure intellectual property (IP) protection through reverse engineering prevention.

### Free Software, Open Source, and Non-Free Open Source Licensing

Open Source software is software who's source code is available to either the public or the consumer of the product. But just because a program is Open Source, does not mean it is "Free". When I use the term "Free", I do not mean Free of charge. Non-Free Software can be distributed for "Free". The term "Free" is referencing the users Freedom.

> “Free software” means software that respects users' freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software.[^freesoftware]

[^freesoftware]: GNU: [What is free software?](https://www.gnu.org/philosophy/free-sw.en.html)

Because of the confusion between "Free" as in free of change and "Free" as in freedom, the community has started to use the word "Libre", which is the Latin work for "Free" in reference to freedom.[^freesoftwareimportant]
Whether or not a developer charges for his program does not depend on whether the software is Free Software or not. In fact the free software foundation encourages developers to charge for their programs.[^sellingfsoft]

[^sellingfsoft]: GNU: [Selling Free Software](https://www.gnu.org/philosophy/selling.html)

Richard Stallman, and the Free software foundation, describe the four essential freedoms[^freesoftwareimportant]:

0. Freedom to run the program as you wish, for whatever purpose.
1. The freedom to study the program's “source code”, and change it, so the program does your computing as you wish.
2. The freedom to make and distribute exact copies when you wish.
3. The freedom to make and distribute copies of your modified versions, when you wish.

### Copy-Left Licensing

Copy-Left Licensing is a form of open source licensing where if the user of a program decides to "fork" or create a derivative program, it must also be open source. One such license is the GNU GPL.[^gnugpl] Using code from a GPL licensed program require the derivative program to be GPL licensed too. Distributing a modified version of a GPL licensed program requires you to distribute the modified source code too. The Linux Kernel, GIMP (GNU Image Manipulation Tool), and Krita are examples of GPL Licensed software

[^gnugpl]: GNU: [General Public License](https://www.gnu.org/licenses/gpl.html)

### Permissive Licensing

This kind of license gives complete freedom to the user. The user can choose to make the program proprietary if they want. The MIT[^mit] and the 3-Clause BSD[^bsd] license are examples of permissive licenses. They are usually the shortest kinds of licenses are offer very little restrictions on the use of the licensed product. Here is the complete MIT license template:

[^bsd]: Opensource.org [BSD](https://opensource.org/licenses/BSD-3-Clause)
[^mit]: Opensource.org [MIT](https://opensource.org/licenses/MIT)

```
Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
### Common Misconceptions

#### Free Software Must be Free of charge!

This is not true at all. Like I said before, "Free" in Free software speaks about your freedom to use the software for any purpose you want. This includes charging for it.[^sellingfsoft] You can, if you want, redistribute paid-for free software free of charge, but there is nothing wrong in charging for it.
An example of this is [Bryan Lunduke's](https://lunduke.com) Game, [Linux Tycoon 2](http://linuxtycoon.com/). It is a GPL licensed game, which he is charging money for.[^lty2]

[^lty2]: Bryan Lunduke [Linux Tycoon 2](http://linuxtycoon.com/)

	I believe in Free Software (Free as in Freedom). And, like Richard Stallman, I believe that just because a piece of software is "Open" or "Libre" does not mean it's "free as in cost". By buying Linux Tycoon 2 you get the full source code with it, licensed under the GPL version 3. This not only ensures that the game can live on long after I've stopped working on it... but it respects your Freedoms.  

#### Source code for free software must be publicly available!

A free software developer does not need to publish the source code to everyone. He only needs to provide the source code to users who also received the binary/executable. If the developer is selling the software, he can decide to only send the software to the people who bought the software.

####

### The importance of Free Software

Non-Free software is an injustice. Free software allows the users, and the community, to control the program. If a program does not give you the basic freedoms, it is mistreating you. Free software allows the users to trust the software as they can look at the code and audit it. They can be sure that the program isn't malicious. Free software means the community can contribute to the software, allowing bugs and features to be fixed and implemented more quickly. Free software allows the users to add or remove features from local copies of the program and share them if they like. Proprietary software often spies on the user. With free software the user can confirm that the software isn't spying on them.[^freesoftwareimportant]

[^freesoftwareimportant]: Richard Stallman: [Free Software Is Even More Important Now](https://www.gnu.org/philosophy/free-software-even-more-important.html)

### Credits

Written with help from [Taniya Dutta](https://lokegaonkar.in/people/taniya), and [Abhay Srivastwa](https://notabhay.xyz).
All sources referenced will be lined in the footnotes.

### Copyright &copy; 2018, 2020, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
