# Working Out and Data

## 21^st^ century: Can't live with offline

I spend an awful about of time in front of my computer, and to balance that I need regular exercise. And I love exercise. But I've realised that exercise is only fun when you have other people to work out with.
Last year I and some of my school friends got together and made a spreadsheet of our workout records. This was extremely fun. We had categories ranging from Cycling, and Yoga to Body Weight. The spreadsheet evolved and became this huge mish-mash of formulae and data. It was amazing. Not only did the competition promote us, but looking at other's records gave us a benchmark to beat.

Unfortunately, it didn't last very long. After 5 months of maintaining the spreadsheet we started fighting over which records were genuine and which records were bogus. I didn't really care that much as long as I could see my records there, but we still ended up abandoning the sheet. And with that I started working out less. It just wasn't that fun anymore.

I know, this sounds weird. Who cares if you're not recording everything you exercise. But I guess [/r/dataisbeautiful](https://reddit.com/r/dataisbeautiful) was spoiled me. So I've decided to take a different approach of recording exercise data: In basic CSV files. Every time I work out, I'll append a line to the respective CSV file. Then I'll set up scripts to parse the files and give me stats about my workout routine. [notabhay](https://notabhay.xyz) will also be joining me, so I'll set up a comparison chart too.

I think I'll also make a "Workout" page on my website with the stats too, once I have enough data. And I'll also share all the scripts on github/gitlabs.

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
