# Moving away from neocities.
## It's not you it's me

I've been mantaining this website for about three months now, and It has been a fun experience. It all started with Chris Were's thoughts on how were bloated and annoying and they don't need to be huge hunks of javascript and css. This website has evolved; in purpose, content, and style. It started out an a basic introduction page for myself, and a place to share my Notes (Which i haven't updated after my exams ended. Sorry!), and then a place to share my contact info. But as the size of the website is increasing it is becoming harder and harder to use Neocities for this website. I have my own server and domain now and this website was available on both [neocities](https://kensp.neocities.org) and [lokegaonkar.in](https://lokegaonkar.in) but it's time to retire neocities. Here are my reasons:

- Having to update my changes in two places in annoying.
- The neocities cli tool does not deal well with a large number of files or large uploads
- I Prefer to use my own domain
- You can only see website stats for the past week. Of course, I do not intend to add any trackers to this website, but the nginx logs are very useful to get an estimate of how many people have viewed my website. (Nice to see, since I'm from the "Big Online Numbers" Generation)
- The ability to run scripts is limited.

I think that about covers it. So, from now on this website will only be available on [lokegaonkar.in](https://lokegaonkar.in)

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
