# Cycles on a mountain
## Never trust the crazy.

A few days ago [Abhay](http://notabhay.xyz) and I decided to "mountain bike" up a hill in Kharghar. I had hiked up this hill before and I knew it was impossible to ride a cycle to the top. It was of course a stupid idea. So we did exactly that, just so we could take pictures. At least it was a great workout.

![Image of Bikes on a hill](/media/bikes.jpg)
![Panorama of Bikes on a hill](/media/pano.jpg)
![Image of me](/media/me.jpg)
![Image of me in a Titanic Pose](/media/tita.jpg)
[Short Video of the hill](/media/hill.mov)

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
