# Pets

## Urine Trouble

Last year my Brother convinced my mother to get us a dog. She was scared of dogs most her life but one day she got drunk and made friend with a rottweiler. Since one of my mother's friend had a dog that she wanted to sell (Her son was having allergies due to it), we bought it. He's named muffin, and he's an English Cockerspaniel.

![Muffin](/media/muffin.jpg)

He's really is cute. Even as a write he's tapping my back for some attention. It's nice to have some company around when you're bored at home.
But, he's trouble. Pissing all over the house, tearing up pillows, digging walls, and "Farming Ticks" all over his body. He is also pretty aggressive towards people he doesn't like. (My Servant Being one of them)

Somehow, even with all that trouble, **Muffin** is totally worth it. I'll be posting pictures of him regularly on this blog. Look forward to it.

> Ruff Ruff
\- Muffin 2018

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
