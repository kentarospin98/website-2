# I don't like air travel

## THE WATER IS DRY!

I'm in a flight from Brussels to Toronto[^connecting] and it's quite miserable. The thing is, there's (mostly[^ignored]) nothing the airline can do to improve my experience. Unless, of course, they upgrade me to business class. 

[^connecting]: I'm travelling from Mumbai to Toronto via Brussels

[^ignored]: Here's the thing. I am annoyed as *heck* with people ignoring me because I look young. This is not an air-travel specific thing, but people just assume I'm accompanying an adult and proceed to become deaf to my words.

Air planes are too cold and dry for me. I get dehydrated very quickly, and it just ruins my whole experience. The worst part is, when you ask for water, they'll never give you a full bottle. Just a single glass.[^water]
The seats aren't very comfortable either, so you can't sleep away the journey. 

[^water]: Sure, I could just ask for another one but I too awkward. Especially, if they start moving.

I'm travelling by Brussels Airlines. And their food is actually great. As for in flight entertainment, they have panels on every seat. I, prefer to carry my own entertainment, A bunch of Lunduke and this blog. 


### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)

