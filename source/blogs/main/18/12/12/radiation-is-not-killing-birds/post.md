# Radiation from mobiles is not killing birds.

## And there is no fifth force of nature.

Sequel to [Robot](https://www.imdb.com/title/tt1305797/), [2.0](https://www.imdb.com/title/tt5080556/) released recently. And while I didn't care much for the story of the movie, which was pretty bad itself, I was furious on how much misinformation the movie had.
Unlike the first movie, 2.0 did a terrible job at separating fact from fiction.
A lot of people don't do research of their own, and believe blindly in pseudoscience spread by movies like this.

### There is no "Good", "Bad", or "Evil" radiation or frequency. Neither does a fifth force or aura exist.

In one of the scenes, a scientist claims that they have engineered a frequency that is "positive" so that they can broadcast it to space such that only aliens with "positive" intentions will be able to intercept it, but ones with "negative" or "evil" intentions will not.
This is one of my biggest pet peeves with pseudoscience.
People throw around terms like "positive and negative energy" and "auras" like they understand how any of this works.
There are only 4 fundamental forces.
Two of them are nuclear forces with a very short range.
The other two are the Electromagnetic force and Gravity.
@funda1
@funda2
@funda3

There is no aura either.
There is no evidence for it, unlike what the movie states. 

### Base Station operators can't just increase the frequency at which that transmit to increase data speed.

Mobile networks operate at very specific range of frequencies, and operators have to license those frequencies from the government to operate on them.
For example, GSM (2G) is operated around 900MHz and 1800MHz.
UMTS (3G) is operated on 900MHz and 2100MHz.
LTE (4G) is operated on frequencies ranging from 850MHz to 2500MHz. WiFi also operates at 2500MHz. 
@freqcheck
@nepalitelecom
@indiatelecom
@wifi
These frequencies are divided into bands and channels.
A telephone operator, such as airtel, cannot increase the transmission frequency to increase audio quality, data speed, or range. 
Because:

1. Higher frequencies might be licensed by other companies, such as vodaphone, or be reserved for government use, such as Railway.
2. It would confuse mobile phones. If the frequencies are out of specification, phones wouldn't be able to connect at all.
3. Higher frequencies would end up reducing the range. High frequency signals provide better link speeds but are more easily absorbed. This is why WiFi is fast but has a very small range, where as GSM (2G) is slow but has a very long range. @freqatt @am1wifi

### Radiation from mobile networks is not strong enough to kill birds, neither is it giving you cancer.

As we discussed before, mobile networks use frequencies in the range of 800-2500MHz. Frequencies in this range are called radio frequencies, and are considered "Non-Ionizing Radiation".
Non-Ionizing means the radiation does not have enough energy to ionize atoms, so they can't modify DNA, or change the chemical structure of your body.
This means radio waves do not cause cancer.
@cancer
The movie show scenes where birds start falling out of the sky and die.
In reality, birds are barely affected by the radiation from mobile networks
Most harm that happens from non-ionizing radiation is cased because of its ability to heat tissue.
This effect is similar to how microwaves heat your food up. 
Though, this effect is minimal at the range and frequencies at which mobile networks operate.
The FDA measures this effect as the Specific Absorption Rate (SAR).
A SAR of 2 is considered safe.
Experiments have shown that, at a distance greater than 4 meters from a base station, SAR values are lower than 2. At 10 meters, they are less than 0.5.
@utahnon 
@radeffbody
@europa

Finally, most birds do not use the earth's magnetic field to migrate.
They use various mechanisms, including the direction of the sun, and local landmarks.
The fields produced by mobile networks do not affect these birds.
Even for birds that do use the earth's magnetic fields, like the homing pigeons, there is no evidence that mobile networks disrupt the ability to detect magnetic fields.
@magnet
@avian

### Conclusion

Do not blindly believe things you see in movies, or read on the internet.
Do your own research, ask experts, read some science textbooks, and most of all, use your common sense. 
Here are a few videos you can checkout on this topic.

- [IDTIMWYTIM: Radiation - SciShow](https://www.youtube.com/watch?v=uJ3ea9fa6CA)
- [Do Cell Phones Cause Cancer? - SciShow](https://www.youtube.com/watch?v=ju2kcMzALkc)
- [Are My Electronics Making Me Sick? - SciShow](https://www.youtube.com/watch?v=3bqLS_OPHXE)
- [Can You Be Allergic to WiFi? - Seeker](https://www.youtube.com/watch?v=MIq16OeLL_E)

::: {#refs}

## References 

:::

### Copyright &copy; 2018, 2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
