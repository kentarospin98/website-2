::: {.content}

::: {.title}
# Welcome to Vikriti D'Vita's Website

### <SPLASH>
:::

::: {.panel}
I'm Vikriti Ishaan D'Vita aka Shanti X, aka kentarospin98. I'm a 23 year old software developer, and artist from Navi Mumbai, India, and this is my personal website.
I'm a non-binary, and I use She/they pronouns.
I studying Computer Science at Arizona State University, in Tempe.
I mod the alphabet mafia [discord server](https://discord.gg/sygRtkq). Feel free to join.
If you're looking for my OpenPGP keys, I've removed that section for now. I have a new key, and I'm resigning everything to reflect that.

If you're looking to hire me for engineering services, or art commisions, you can [email me at v@lokegaonkar.in](mailto:v@lokegaonkar.in).
:::

<!-- 
::: {.panel}
## OpenPGP keys

I use openPGP keys to sign and encrypt my emails, messages, commits and blogs. You can find the key [here](/media/key.txt). The key fingerprint is CC5E 77FF 0080 73C1 C3C4 F849 6D2D CD5C 0EA8 04B6. You can also find my keys on keyservers. Be sure to verify the key fingerprint.
::: -->

::: {.panel}
## Blog

Looks like all the cool kids have a blog. So I do too. Check them out [Here](/blogs/main). I have PDF and plain text version of the next available. I also have an RSS feed for the blog. [Subscribe here!](/feeds/main.xml)
:::

::: {.panel}
## Featured Posts

[Radiation from mobiles is not killing birds.](/blogs/main/18/12/12/radiation-is-not-killing-birds/)

[Free Software: And why you should pay for it](/blogs/main/18/05/28/free-software/)

[Rights of Sex Workers in India](/blogs/main/18/09/21/rights-of-sex-workers-in-india/)

:::

::: {.panel}
# Featured Websites

- [Chris Were's Website](https://chriswere.neocities.org/)
- [Amolinux's Website](https://superuser.neocities.org/)
:::

::: {.panel}
### Copyright &copy; 2018-2023 Vikriti D'Vita

This work is licensed under [Creative Commons Attribution 4.0 International License.](https://creativecommons.org/licenses/by/4.0/)
:::


:::

::: {.footer}

::: {.contact}
- [![envolope](/images/envelope.svg){.icon} [Email Me at v@lokegaonkar.in]{.cinfo}](mailto:v@lokegaonkar.in)
- [![gitlab](/images/gitlab.svg){.icon} [Check out my Gitlab profile]{.cinfo}](https://gitlab.com/kentarospin98)
- [![github](/images/github.svg){.icon} [Check out my Github profile]{.cinfo}](https://github.com/kentarospin98)
- [![mastodon](/images/masto.svg){.icon} [Follow me on Mastodon]{.cinfo}](https://linuxrocks.online/@kensp){rel="me"}
- [![twitter](/images/twitter.svg){.icon} [Follow me on Twitter]{.cinfo}](https://twitter.com/Imshantix)
:::

:::
