#!/usr/bin/bash

website="https://lokegaonkar.in"

genpdf() {
	local sourcefile=$1;
	local outputfile=$2;
	local bibliographyfile=$3
	if [ -f "$bibliographyfile" ]; then
		local bibliographyflags="--citeproc --csl $cslfile --bibliography $bibliographyfile --metadata link-citations";
	fi
	local tempfile=$tempdir/tempmd;
	if [ -f "$tempfile" ];then
		rm $tempfile;
	fi;
	cat $sourcefile | sed "s/\(\!\[.*(\)\/\(.*\)/\1.\/build\/\2{width=80%}/"  > $tempfile
	pandoc -f markdown -t latex $bibliographyflags $tempfile -o $outputfile;
	if [ ! -f "$outputfile" ]; then
		printf "PDF generation for $sourcefile Failed\n"
	fi;
}

genplaintext() {
	local sourcefile=$1;
	local outputfile=$2;
	if [ -n "$5" ]; then
		local bibliographyfile=$3
		local header=${4:-$sourcedir/${headerfile:-templates/header}};
		local footer=${5:-$sourcedir/${footerfile:-templates/footer}};
	else
		local header=${3:-$sourcedir/${headerfile:-templates/header}};
		local footer=${4:-$sourcedir/${footerfile:-templates/footer}};
	fi
	if [ -f "$bibliographyfile" ]; then
		local bibliographyflags="--citeproc --csl $cslfile --bibliography $bibliographyfile --metadata link-citations";
	fi
	pandoc -f markdown -t plain $bibliographyflags $sourcefile -o $outputfile;
}

genhtml() {
	local sourcefile=$1;
	local outputfile=$2;
	if [ -n "$5" ]; then
		local bibliographyfile=$3
		local header=${4:-$sourcedir/${headerfile:-templates/header}};
		local footer=${5:-$sourcedir/${footerfile:-templates/footer}};
	else
		local header=${3:-$sourcedir/${headerfile:-templates/header}};
		local footer=${4:-$sourcedir/${footerfile:-templates/footer}};
	fi
	if [ -f "$bibliographyfile" ]; then
		local bibliographyflags="--citeproc --csl $cslfile --bibliography $bibliographyfile --metadata link-citations";
	fi
	local tempfile=$tempdir/temphtml;
	if [ -f "$tempfile" ];then
		rm $tempfile;
	fi;
	pandoc -f markdown -t html4 $bibliographyflags $sourcefile -o $tempfile;
	if [ -f "$header" ];then
		cat $header >> $outputfile;
	else
		printf "<meta charset=UTF-8 /><html><head><title>Index</title></head><body>" >> $outputfile
	fi;
	cat $tempfile >> $outputfile
	if [ -f "$footer" ];then
		cat $footer >> $outputfile;
	else
		printf "</body></html>" >> $outputfile
	fi;
}

genindex() {
	local filelistrev=( ${!1} );
	local filelist=($(printf "%s\n" ${filelistrev[@]}  | tac | tr "\n" " "; echo))
	local outputfile=$2;
	local blogname=$3;
	printf "\n::: {.content}\n" >> $outputfile
	for file in ${filelist[@]}; do
		local postname=$(basename ${file%/*});
		local metadatafile="$tempdir/$postname-metadata"
		local metadata;
		readarray -t metadata < $metadatafile;
		local year=${metadata[2]};
		local month=${metadata[1]};
		local day=${metadata[0]};
		local title=$(head -n 1 $file);
		local postdir="$blogdir/$year/$month/$day/$postname/";
		local postlink=${postdir#./build};
		local head="# [${title#\# }]($postlink)";
		local date="## $day-$month-$year";
		local preview="$(head -n 5 $file | sed '/#/d')";
		printf "\n::: {.panel}\n\n$head\n\n$date\n\n$preview\n\n:::\n" >> $outputfile;
	done;
	printf "\n:::\n" >> $outputfile
}

genrss() {
	local filelistrev=( ${!1} );
	local filelist=($(printf "%s\n" ${filelistrev[@]}  | tac | tr "\n" " "; echo))
	local outputfile=$2;
	local blogname=$3;
	local blogmetadatafile=$4;
	local blogmetadata;
	readarray -t blogmetadata < $blogmetadatafile;
	local blogtitle=${blogmetadata[0]:-TITLE};
	local blogdesc=${blogmetadata[1]:-DESC};
	local bloglang=${blogmetadata[2]:-en-US};
	printf "<?xml version='1.0' encoding='UTF-8' ?>\n" >> $outputfile;
	printf "<rss version='2.0'>\n" >> $outputfile;
	printf "<channel>\n" >> $outputfile;
	printf "<title>$blogtitle</title>" >> $outputfile;
	printf "<!-- <atom:link href='https://lokegaonkar.in/feed/$blogname.xml' rel='self' type='application/rss+xml' /> -->\n" >> $outputfile;
	printf "<link>https://lokegaonkar.in</link>\n" >> $outputfile;
	printf "<description>$blogdesc</description>\n" >> $outputfile;
	printf "<language>$bloglang</language>\n" >> $outputfile;
	for file in ${filelist[@]}; do
		local postname=$(basename ${file%/*});
		metadatafile="$tempdir/$postname-metadata"
		local metadata;
		readarray -t metadata < $metadatafile;
		local year=${metadata[2]};
		local month=${metadata[1]};
		local day=${metadata[0]};
		local title=$(head -n 1 $file);
		local postdir="$blogdir/$year/$month/$day/$postname/";
		local postlink=${postdir#./build};
		printf "<item>\n" >> $outputfile;
		printf "<title>${title#\# }</title>\n" >> $outputfile;
		printf "<description><![CDATA[" >> $outputfile;
		cat $postdir/index.html >> $outputfile 
		printf "]]></description>\n" >> $outputfile;
		printf "<pubDate>$(date -R --date=$year-$month-$day)</pubDate>\n" >> $outputfile;
		printf "<guid isPermaLink='false'>$website/blogs/$blognamehtml/$year/$month/$date/$postname</guid>\n" >> $outputfile;
		printf "</item>\n" >> $outputfile;
	done;
	printf "</channel>\n" >> $outputfile
	printf "</rss>\n" >> $outputfile
}

genblog() {
	local blogsource=$1;
	local blogdir=$2;
	local blogname=$(basename $blogdir);
	local cslfile=$sourcedir/csl/mla.csl;
	printf "Generating $(basename $blogsource)\n";
	mkdir $blogdir;
	mkdir $builddir/feeds;
	local allposts=();
	for yearfol in $blogsource/*; do
		[ -e "$yearfol" ] || continue
		local yearposts=();
		local year=$(basename $yearfol);
		mkdir "$blogdir/$year";
		for monthfol in $yearfol/*; do
			[ -e "$monthfol" ] || continue
			local monthposts=();
			local month=$(basename $monthfol);
			mkdir "$blogdir/$year/$month"
			for dayfol in $monthfol/*; do
				[ -e "$dayfol" ] || continue
				local dayposts=();
				local day=$(basename $dayfol)
				mkdir "$blogdir/$year/$month/$day"
				for postfol in $dayfol/*; do
					[ -e "$postfol" ] || continue
					postname=$(basename $postfol);
					mkdir "$blogdir/$year/$month/$day/$postname"
					printf "$year $month $day $postname\n";
					local header=$sourcedir/${headerfile:-templates/blogheader};
					local footer=$sourcedir/${footerfile:-templates/blogfooter};
					local tempfile=$tempdir/tempmd;
					local dayposts[${#dayposts[*]}]=$postfol/post.md;
					local metadata="$tempdir/$postname-metadata"
					printf "$day\n" >> $metadata
					printf "$month\n" >> $metadata
					printf "$year\n" >> $metadata
					if [ -f $postfol/noPDF ]; then
						printf "Skipping PDF Generation: noPDF Flag Found\n"
					elif [ -f $postfol/post.pdf ]; then
						printf "Skpiing PDF Generation: Copying Existing PDF\n"
						cp $postfol/post.pdf "$blogdir/$year/$month/$day/$postname/$postname.pdf";
					else
						genpdf $postfol/post.md "$blogdir/$year/$month/$day/$postname/$postname.pdf" "$postfol/post.bib" ;
					fi
					genplaintext $postfol/post.md "$blogdir/$year/$month/$day/$postname/$postname.txt" "$postfol/post.bib" $header $footer
					if [ -f $tempfile ]; then
						rm $tempfile
					fi
					cat $postfol/post.md >> $tempfile
					printf "\n---\n" >> $tempfile
					printf "\n### Get the [markdown](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/post.md)\n\n" >> $tempfile
					if [ -f "$postfol/post.md.sig" ]; then
						cp $postfol/post.md.sig "$blogdir/$year/$month/$day/$postname/"
						printf "\n### Get the Markdown [signature](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/post.md.sig)\n\n" >> $tempfile
					fi
					if [ -f "$postfol/post.bib" ]; then
						cp $postfol/post.bib "$blogdir/$year/$month/$day/$postname/"
						printf "\n### Get the [Bibliography](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/post.md.sig)\n\n" >> $tempfile
					fi
					if [ -f "$postfol/post.bib.sig" ]; then
						cp $postfol/post.bib.sig "$blogdir/$year/$month/$day/$postname/"
						printf "\n### Get the Bibliography [signature](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/post.md.sig)\n\n" >> $tempfile
					fi
					if [ -f "$blogdir/$year/$month/$day/$postname/$postname.pdf" ]; then
						printf "\n### Get the [PDF Version](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/$postname.pdf)\n\n" >> $tempfile
					fi
					if [ -f "$blogdir/$year/$month/$day/$postname/$postname.txt" ]; then
						printf "\n### Get the [Plain Text Version](/blogs/$(basename $blogsource)/$year/$month/$day/$postname/$postname.txt)\n\n" >> $tempfile
					fi
					printf "\n---\n# [Subscribe to my blog using RSS]($website/feeds/$blogname.xml)" >> $tempfile
					genhtml $tempfile "$blogdir/$year/$month/$day/$postname/index.html" "$postfol/post.bib" $header $footer
					cp $postfol/post.md "$blogdir/$year/$month/$day/$postname/"
				done;
				genindex dayposts[@] "$blogdir/$year/$month/$day/index.md"
				genhtml "$blogdir/$year/$month/$day/index.md" "$blogdir/$year/$month/$day/index.html"
				monthposts+=(${dayposts[@]})
			done
			genindex monthposts[@] "$blogdir/$year/$month/index.md" $blogname
			genhtml "$blogdir/$year/$month/index.md" "$blogdir/$year/$month/index.html"
			yearposts+=(${monthposts[@]})
		done
		genindex yearposts[@] "$blogdir/$year/index.md" $blogname
		genhtml "$blogdir/$year/index.md" "$blogdir/$year/index.html"
		allposts+=(${yearposts[@]})
	done
	genindex allposts[@] "$blogdir/index.md" $blogname
	genhtml "$blogdir/index.md" "$blogdir/index.html"
	genrss allposts[@] "$builddir/feeds/$blogname.xml" $blogname "$blogsource/metadata"
}

gen() {
	sourcedir=${sourcedir:-./source};
	builddir=${builddir:-./build};
	tempdir=${tempdir:=./temp};
	if [ -d $tempdir ];then
		rm -r $tempdir;
	fi;
	if [ -d $builddir ];then
		rm -r $builddir;
	fi;
	mkdir $tempdir;
	mkdir $builddir;
	printf "Copying Contact folder";
	contacts=$sourcedir/${contactdir:-contact}
	if [ -d $contacts ]; then
		printf "..";
		cp $contacts $builddir/contact -r;
		printf ".";
	else
		printf "... No Contact Folder Found ..."
	fi
	printf "done\n"
	printf "Generating Index";
	index=$sourcedir/${indexfile:-index.md};
	splash=$sourcedir/${splashfile:-splash.txt};
	if [ -f $index ]; then
		printf "."
		if [ -f $splash ];then
			RANDOMSPLASH=$(shuf -n 1 $splash);
			sed "s/<SPLASH>/$RANDOMSPLASH/g" < $index > $tempdir/indexsplashed.md;
		else
			sed "s/.*<SPLASH>.*//g" < $index > $tempdir/indexsplashed.md;
		fi;
		printf "."
		genhtml $tempdir/indexsplashed.md $builddir/index.html;
		printf "."
	fi;
	printf "done\n";
	printf "Copying Stylesheet";
	local stylesheet=$sourcedir/${stylesheetfile:-style.css};
	if [ -f $stylesheet ];then
		printf "."
		cp $stylesheet $builddir/style.css;
		printf ".."
	else
		printf "... No Stylesheet Found ..."
	fi;
	printf "done\n";
	printf "Copying Images";
	local images=$sourcedir/${imagedir:-images};
	if [ -d $images ];then
		printf ".";
		cp -r $images $builddir/images;
		printf "..";
	else
		printf "... No Images Found ...";
	fi;
	printf "done\n";
	printf "Copying Media Directory";
	local media=$sourcedir/${mediadir:-media};
	if [ -d $media ];then
		printf ".";
		cp -r $media $builddir/media;
		printf "..";
	else
		printf "... No Media Directory Found ...";
	fi;
	printf "done\n";
	printf "Copying Fonts";
	fonts=$sourcedir/${imagedir:-fonts};
	if [ -d $fonts ];then
		printf ".";
		cp -r $fonts $builddir/fonts;
		printf "..";
	else
		printf "... No Fonts Found ...";
	fi;
	printf "done\n";
	printf "Generating Blogs\n";
	blogcollection=$sourcedir/${blogcollectiondir:-blogs}
	if [ -d $blogcollection ]; then
		mkdir $builddir/blogs;
		for blogdir in $blogcollection/*; do
			if [ -d $blogdir ]; then
				genblog $blogdir $builddir/blogs/$(basename $blogdir);
			else
				[ $blogdir -eq metadata ] && continue;
				printf "... No Blogs Found ..."l
			fi;
		done
	fi
	printf "done\n"
}

printf "Welcome to webctl 2\n"
gen
